<?php

namespace App\Http\Controllers;

use Portal\Ppj\Http\Controllers\DashboardController;

class HomeController extends Controller
{
    public function __construct(DashboardController $dashboard)
    {

         $this->dashboard = $dashboard;

    }


    public function __invoke()
    {
    	
        return $this->dashboard->index();
    }
}
