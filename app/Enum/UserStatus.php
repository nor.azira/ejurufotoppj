<?php

namespace App\Enum;

use BenSampo\Enum\Enum;

final class UserStatus extends Enum
{
    const INACTIVE = 'INACTIVE';
    const ACTIVE = 'ACTIVE';
}
