<?php

namespace App\Services;

use Adldap\Models\User;
use Illuminate\Support\Facades\Hash;
use Portal\Support\Enum\UserStatus;

class EloquentUserResolver
{
    public function resolve(User $ldapUser, $data)
    {
        $attributes = $ldapUser->getAttributes();
        $password = array_get($data, 'password');

        $ldaparray = [];

        if (isset($attributes['name']['0'])) {
            $ldaparray['fullname'] = $attributes['name']['0'];
        }
        if (isset($attributes['samaccountname']['0'])) {
            $ldaparray['username'] = $attributes['samaccountname']['0'];
        }
        if (isset($attributes['mail']['0'])) {
            $ldaparray['mail'] = $attributes['mail']['0'];
        }
        if (isset($attributes['title']['0'])) {
            $ldaparray['title'] = $attributes['title']['0'];
        }
        if (isset($attributes['physicaldeliveryofficename']['0'])) {
            $ldaparray['office'] = $attributes['physicaldeliveryofficename']['0'];
        }
        if (isset($attributes['department']['0'])) {
            $ldaparray['department'] = $attributes['department']['0'];
        }
        if (isset($attributes['manager']['0'])) {
            $ldaparray['manager'] = $attributes['manager']['0'];
        }
        if (isset($attributes['thumbnailphoto']['0'])) {
            $ldaparray['thumbnailphoto'] = base64_encode($attributes['thumbnailphoto']['0']);
        }else{
            $ldaparray['thumbnailphoto'] = 'null';
        }

        if (($identifier = array_get($ldaparray, 'mail')) === null) {
            return null;
        }

        $eloquentUser = \App\User::firstOrNew(
            [
                'email' => $identifier,
            ],
            [
                'name'     => array_get($ldaparray, 'fullname', $identifier),
                'password' => Hash::make($password),
                'status'   => UserStatus::ACTIVE,
                'timezone' => config('app.timezone'),
            ]
        );

        $eloquentUser->meta['ldap'] = $ldaparray;
        $eloquentUser->save();

        return $eloquentUser;
    }
}
