<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_app', function (Blueprint $table) {
            $table->increments('id');
            $table->string('staff_id',55)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('appl_type_id')->unsigned()->nullable();
            $table->date('appl_date')->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->integer('supported_by')->unsigned()->nullable();
            $table->date('supported_date')->nullable();
            $table->string('supported_remark',255)->nullable();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->date('approved_date')->nullable();
            $table->string('approved_remark',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_app');
    }
}
