<?php

use Illuminate\Database\Seeder;

class UserAndRoleSeeder extends Seeder
{
    protected function roles()
    {
        return [
            'Administrator' => app('laravolt.acl')->permissions(),
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles() as $role => $permissions) {
            $role = \Laravolt\Acl\Models\Role::firstOrCreate(['name' => $role]);
            $role->syncPermission($permissions);
            $user = factory(\App\User::class)->create(
                [
                    'email'  => sprintf('%s@test.app', strtolower($role->name)),
                    'name'   => $role->name,
                    'status' => \App\Enum\UserStatus::ACTIVE,
                ]
            );
            $user->assignRole($role);
        }
    }
}
