<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LkpStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lkp_status')->insert([
            [
            	'description' => 'Draf Permohonan',
            	'status' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now()
            ],
            [
            	'description' => 'Permohonan Kursus Baru',
            	'status' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now()
            ],
            [
            	'description' => 'Permohonan Kursus Disokong',
            	'status' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now()
            ],
            [
            	'description' => 'Permohonan Kursus Tidak Disokong',
            	'status' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now()
            ],
            [
            	'description' => 'Permohonan Kursus Diluluskan',
            	'status' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now()
            ],
            [
            	'description' => 'Permohonan Kursus Tidak Diluluskan',
            	'status' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now()
            ]

            
        ]);

    }
}
