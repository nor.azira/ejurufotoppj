@extends('ui::layouts.base')

@section('body')
    <div class="layout--app">

        <div class="content" style="background-image: url('/img/bg.png')">
            <div class="content__inner">
                <div class="ui container-fluid content__body p-1" >
                    @yield('content')
                </div>

            </div>
        </div>
    </div>
@endsection
