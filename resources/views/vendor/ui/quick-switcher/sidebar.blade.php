
<div class="ui column grid">
  <div class="column">
    <div class="ui raised segment" style="margin-left: 15px;">
      <a class="ui red ribbon label">{{ auth()->user()->name }}</a>
      <p></p>
      <a class="ui yellow image label">
      		<img src="/laravolt/img/bg/index.png">Role
  			<div class="detail">{{auth()->user()->roles['0']->name}}</div>
	  </a>
      <p></p>
    <a href="{{ route('auth::logout') }}" class="ui teal left ribbon label">Logout</a>
      <p></p>
    </div>
  </div>
 </div>


<!-- <div class="ui attached vertical menu fluid" data-role="quick-menu">
    <div class="item">
        <div class="ui icon input">
            <input type="text" placeholder="" data-role="quick-menu-searchbox">
            <i class="search icon"></i>
        </div>
    </div>
    <div class="items ui accordion sidebar__accordion">

    </div>
</div>
 -->