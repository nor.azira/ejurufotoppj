@if(strlen(config('laravolt.ui.brand_image')) > 0)
    <img src="/laravolt/img/bg/index.png" alt="" class="ui image {{ $class ?? 'tiny' }}">
@endif
