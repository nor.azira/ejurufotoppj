<header class="ui menu small borderless fixed top {{ config('laravolt.ui.options.topbar_inverted') ? 'inverted': '' }}" style="-webkit-box-shadow: 0 1px 2px 0 rgba(34,36,38,.15);">
    <div class="item mobile only tablet only" data-role="sidebar-visibility-switcher"><i class="icon sidebar"></i></div>
    {{ Breadcrumbs::exists() ? Breadcrumbs::render() : null }}
   <!--  <div class="menu right p-r-1">
        <div class="ui item dropdown simple right">
            <img src="{{ \Laravolt\Avatar\Facade::create(auth()->user()->name)->toBase64() }}" alt="" class="ui image avatar">
            {{ auth()->user()->name }}
            <i class="icon dropdown"></i>
            <div class="menu">
                <a href="{{ route('auth::logout') }}" class="item">Logout</a>
            </div>
        </div>

    </div> -->
</header>
