@extends('errors::rendertohome')

@section('code', '429')
@section('title', __('Too Many Requests'))


@section('message', __('Harap Maaf, Terlalu Banyak Permintaan Akses Ke Pelayan Applikasi Ketika ini, Sila Cuba Sebentar Lagi'))
