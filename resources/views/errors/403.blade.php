@extends('errors::rendertohome')

@section('code', '403')
@section('title', __('Forbidden'))

@section('message', __($exception->getMessage() ?: 'Harap Maaf, Anda Tidak Dibenarkan Untuk Akses Paparan Ini'))
