@extends('errors::render')

@section('code', 'E-Cuti')
@section('title', __('Sesi Web Tamat'))


@section('message')
Harap Maaf, Sesi Akses Anda Telah Tamat, Sila login Semula

@endsection

