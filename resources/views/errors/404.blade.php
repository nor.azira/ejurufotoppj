@extends('errors::rendertohome')

@section('code', '404')
@section('title', __('Page Not Found'))

@section('message', __('Harap Maaf, Halaman Yang Anda Cari Tidak Dijumpai'))
