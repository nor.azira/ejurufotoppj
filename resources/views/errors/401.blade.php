@extends('errors::rendertohome')

@section('code', '401')
@section('title', __('Unauthorized'))

@section('message', __('Harap Maaf, Anda Tidak Dibenarkan Untuk Akses Paparan Ini''))
