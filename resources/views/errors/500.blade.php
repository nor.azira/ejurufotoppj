@extends('errors::rendertohome')

@section('code', '500')
@section('title', __('Error'))

@section('message', __('Harap Maaf, Terdapat Masalah Teknikal Di Pelayan Applikasi Kami, Sila Cuba Sebentar Lagi'))
