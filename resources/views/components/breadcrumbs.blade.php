<div class="item">
    @if (count($breadcrumbs))
        <div class="ui breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                @if (strtolower($breadcrumb->title) === 'home')
                    <a href="{{ $breadcrumb->url }}" class="section">
                        <i class="icon building outline"></i>
                        @lang('Home')
                    </a>
                    @if (!$loop->last)
                        <i class="divider right angle icon"></i>
                    @endif
                @elseif(!$loop->last)
                    @if ($breadcrumb->url)
                        <a href="{{ $breadcrumb->url }}" class="section">{{ $breadcrumb->title }}</a>
                    @else
                        <div class="section">{{ $breadcrumb->title }}</div>
                    @endif
                    <i class="divider right angle icon"></i>
                @else
                    <div class="section">{{ $breadcrumb->title }}</div>
                @endif
            @endforeach
        </div>
    @endif
</div>
