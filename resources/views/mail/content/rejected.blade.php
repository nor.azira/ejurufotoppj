@component('mail::message')
Dear {{ $data->user->name}},

Your uploaded photo is accepted.

Please Log-in to view the changes

Thank you,

This is automated notification from IIUM Photoxcess.
@endcomponent
