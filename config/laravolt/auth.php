<?php

return [
    'layout'       => 'ui::layouts.auth',
    'captcha'      => false,
    'identifier'   => 'email',
    'login' => [
        'implementation' => \Laravolt\Auth\DefaultLogin::class,
    ],
    'registration' => [
        'enable'         => false,
        'status'         => 'ACTIVE',
        'implementation' => \Laravolt\Auth\DefaultUserRegistrar::class,
    ],
    'activation'   => [
        'enable'        => false,
        'status_before' => 'PENDING',
        'status_after'  => 'ACTIVE',
    ],
    'cas'  => [
        'enable' => false,
    ],
    'ldap' => [
        'enable'   => false,
        'resolver' => [
            'eloquent_user' => \App\Services\EloquentUserResolver::class,
            'ldap_user'     => \App\Services\LdapUserResolver::class,
        ],
    ],
    'router'       => [
        'middleware' => ['web'],
        'prefix'     => 'auth',
    ],
    'redirect'     => [
        'after_login'          => '/',
        'after_register'       => '/',
        'after_reset_password' => '/',
    ],
];
