<?php

Route::group(
    [
        'namespace'  => '\Portal\Ppj\Http\Controllers',
        'prefix'     => '',
        'as'         => 'ppj::',
        'middleware' => ['web', 'auth'],
    ],
    function () {
    	
         Route::get('home',['uses' => 'DashboardController@index', 'as' => 'dashboard.index']);
         Route::get('isiform',['uses' => 'FormController@isiform', 'as' => 'form.isiform']);
         Route::post('simpanborang',['uses' => 'FormController@simpanborang', 'as' => 'form.simpanborang']);


    }
);


//enable this if you want 
// Route::group(
//     [
//         'namespace'  => '\Portal\Ppj\Http\Controllers',
//         'prefix'     => '',
//         'as'         => 'web::',
//         'middleware' => ['web'],
//     ],
//     function () {
        

//         Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home.index']);

//     }
// );
