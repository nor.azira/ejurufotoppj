<?php

namespace Portal\Ppj\Http\Controllers;

use Illuminate\Routing\Controller;
use Carbon\Carbon;
use DB;
use Portal\Ppj\Model\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use File;
use Redirect;
use App\Mail\StatusAccept;
use App\Mail\StatusReject;
use Mail;
use Curl;
use Portal\Ppj\Model\MasterApp;


class FormController extends Controller
{


    public function isiform()
    {

               $user = Users::
                            where('users.email', Auth::User()->email)
                            ->leftJoin('vw_staff_federateds', 'users.username', '=', 'vw_staff_federateds.username')
                            ->first();
                

        	   return view('ppj::form.borangpermohonan',compact('user'));
      
    }


     public function simpanborang(Request $request)
    {

          $userid = auth()->user()->id;
          $data = new MasterApp;
          $data->tujuanprogram = $request->tujuanprogram;
          $data->lokasiprogram = $request->lokasiprogram;
          $data->tarikhprogram = $request->date;
          $data->notelbimbit = $request->notelbimbit;
          $data->fk_user = $userid;
          $data->status = 1;
          $data->save();

          
          return redirect('/home');   
      
    }

}
