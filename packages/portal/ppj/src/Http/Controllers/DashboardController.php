<?php

namespace Portal\Ppj\Http\Controllers;

use Illuminate\Routing\Controller;
use Carbon\Carbon;
use DB;
use Portal\Ppj\Model\Users;
use Portal\Ppj\Model\MasterApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Redirect;
use App\Mail\StatusAccept;
use App\Mail\StatusReject;
use Mail;
use Curl;

class DashboardController extends Controller
{


    public function index()
    {

        // contoh commit
       
        $items = array();
    	$events = array();
        $keyword = request('search');
    	
    	// $staff  = $this->stafrepo->info(\Auth::user()->employeeCode);

        $now = Carbon::now();

        $roleid = auth()->user()->roles['0']->id;

        // dd($roleid);

        if ( $roleid == '1') {

        	   return view('ppj::dashboard.admin');
        }
        if (($roleid == '2' ) OR ($roleid == '5' )) {
                            
            $list = Users::
                select('users.name', 'vw_staff_federateds.department_name', 'vw_staff_federateds.division_name', 'vw_staff_federateds.employeeCode', 'vw_staff_federateds.office_no')
                ->leftJoin('vw_staff_federateds', 'users.username', '=', 'vw_staff_federateds.username')
                ->where('users.name', 'like', '%'.$keyword.'%')
                ->orWhere(
                    function ($query) use ($keyword) {
                        $query->where('users.email', 'like', '%'.$keyword.'%');
                    }
                )->paginate(10);

                $id = auth()->user()->id;
                $data = MasterApp::where('fk_user', $id)->where('status', 1)->get();
        	return view('ppj::dashboard.pemohon',compact('list','data'));


        }if ($roleid == '3') {


        	  return view('ppj::dashboard.verifier');
        }
        if ($roleid == '4') {


        	  return view('ppj::dashboard.approval');
        }

      
    
      
    }

}
