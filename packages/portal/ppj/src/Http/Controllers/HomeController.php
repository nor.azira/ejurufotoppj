<?php

namespace Portal\Ppj\Http\Controllers;

use Illuminate\Routing\Controller;
use Carbon\Carbon;
use DB;
use Portal\Ppj\Model\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Redirect;
use App\Mail\StatusAccept;
use App\Mail\StatusReject;
use Mail;
use Curl;

class HomeController extends Controller
{


    public function index()
    {

       return view('ppj::home.index');
        // }
    }

}
