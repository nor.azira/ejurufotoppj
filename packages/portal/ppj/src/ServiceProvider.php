<?php

namespace Portal\Ppj;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use App\Enums\Permission;

/**
 * Class PackageServiceProvider
 *
 * @package Portal\Ecuti
 * @see http://laravel.com/docs/master/packages#service-providers
 * @see http://laravel.com/docs/master/providers
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @see http://laravel.com/docs/master/providers#deferred-providers
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @see http://laravel.com/docs/master/providers#the-register-method
     * @return void
     */
    public function register()
    {
        // $this->app->bind('staff', \Portal\Ecuti\Data\Repo\StaffRepo::class);
    }

    /**
     * Application is booting
     *
     * @see http://laravel.com/docs/master/providers#the-boot-method
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'ppj');
        $this->menu();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    protected function menu()
    {
              
        $menu = $this->app['laravolt.menu']->add(__('Dashboard'))->data('icon', 'align justify');
        $menu->add(__('Dashboard'), url('/home/'))
             ->data('icon', 'circle outline')
             ->active('home/*');

        $menu->add(__('Borang Permohonan'), route('ppj::form.isiform'))
        ->data('icon', 'circle outline')
        ->active('isiform/*');
        
        //      ->data('permission', Permission::MAKLUMAT_CUTI);
        // $menu->add(__('Maklumat Cuti'), route('ecuti::quota.index'))
        //      ->data('icon', 'circle outline')
        //      ->active('quota/*')
        //      ->data('permission', Permission::MAKLUMAT_CUTI);
    }


}