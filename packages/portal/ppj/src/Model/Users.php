<?php

namespace Portal\Ppj\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Users extends Model
{

    use Notifiable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function info()
    {
       
    }

    public function vw_staff_federateds()
    {
        return $this->belongsTo('Portal\Ppj\Model\VwStaffFederateds','username');
    }

    public function master_app()
    {
        return $this->belongsTo('Portal\Ppj\Model\MasterApp','fk_user');
    }
}
