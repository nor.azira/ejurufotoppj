<?php

namespace Portal\Ppj\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VwStaffFederateds extends Model
{

    use Notifiable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vw_staff_federateds';


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function info()
    {
       
    }

    
}
