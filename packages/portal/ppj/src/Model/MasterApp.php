<?php

namespace Portal\Ppj\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class MasterApp extends Model
{

    use Notifiable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_app';


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function info()
    {
       
    }

    public function users()
    {
        return $this->belongsTo('Portal\Ppj\Model\Users','fk_user');
    }

}
