<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_app', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_user')->nullable();
            $table->text('tujuanprogram')->nullable();
            $table->text('lokasiprogram')->nullable();
            $table->text('notelbimbit')->nullable();
            $table->text('tarikhprogram')->nullable();
            $table->text('jenispermohonan')->nullable();
            $table->integer('idpelulusjabatan')->nullable();
            $table->text('tarikhpelulusjabatan')->nullable();
            $table->text('idpelulusbkk')->nullable();
            $table->text('tarikhpelulusbkk')->nullable();
            $table->text('ulasanpelulusbkk')->nullable();
            $table->text('idjurufoto')->nullable();
            $table->integer('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_app');
    }
}
