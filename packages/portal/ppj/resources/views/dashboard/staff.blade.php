@extends('ui::layouts.app')

@section('content')
<div class="ui segment">
    <a class="ui blue left ribbon label">Selamat Kembali</a>
    <br>
    <br>
	<div class="column sixteen wide">
	    <div class="ui cards four doubling">
		    <a href="/home" class="ui card">
		        <div class="content">
		            <h3 class="header link">Permohonan Kursus Baru</h3>
		        </div>
		        <div class="extra content">
		           
		            <span class="right floated"><i class="icon options"></i> 3</span>
		        </div>
		   </a>
		    <a href="http://latihan.devl/epicentrum/roles/2/edit" class="ui card">
		        <div class="content">
		            <h3 class="header link">Permohonan Kursus Disokong</h3>
		        </div>
		        <div class="extra content">
		          
		            <span class="right floated"><i class="icon options"></i> 1</span>
		        </div>
		    </a>
		    <a href="http://latihan.devl/epicentrum/roles/3/edit" class="ui card">
		        <div class="content">
		            <h3 class="header link">Permohonan Kursus Diluluskan</h3>
		        </div>
		        <div class="extra content">
		          
		            <span class="right floated"><i class="icon options"></i> 0</span>
		        </div>
		    </a>
		    <a href="http://latihan.devl/epicentrum/roles/4/edit" class="ui card">
		        <div class="content">
		            <h3 class="header link">Sejarah Kehadiran Kursus</h3>
		        </div>
		        <div class="extra content">
		           
		            <span class="right floated"><i class="icon options"></i> 0</span>
		        </div>
		    </a>
	    </div>
	</div>
	<br>
	<br>
	<a class="ui blue left ribbon label">Permohonan Kursus Baru</a>
	<br>
	<br>
	    {!! 
    Suitable::source($list)
        ->title(__('Senarai Permohonan Kursus (Deraf)'))
        ->columns([
            new \Laravolt\Suitable\Columns\Numbering(__('No')),
            ['header' => __('name'), 'field' => 'name', 'sortable' => true]
       
            
        ])
        ->render()
    !!}
</div>
   

@endsection
