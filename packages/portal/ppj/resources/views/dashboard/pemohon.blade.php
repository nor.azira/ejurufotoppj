@extends('ui::layouts.app')

@section('content')
<div class="ui segment">
    <a class="ui blue left ribbon label">Selamat Kembali</a>
    <br>
    <br>
	<div class="column sixteen wide">
	    <div class="ui cards four doubling">
		    <a href="/home" class="ui card">
		        <div class="content">
		            <h3 class="header link">Senarai Permohonan</h3>
		        </div>
		        <div class="extra content">
		           
		            <span class="right floated"><i class="icon options"></i> {{$data->where('status',1)->count()}}</span>
		        </div>
		   </a>
		    <a href="/home" class="ui card">
		        <div class="content">
		            <h3 class="header link">Status Permohonan</h3>
		        </div>
		        <div class="extra content">
		          
		            <span class="right floated"><i class="icon options"></i>  {{$list->count()}}</span>
		        </div>
		    </a>
		    <a href="/home" class="ui card">
		        <div class="content">
		            <h3 class="header link">Permohonan Jurufoto Diluluskan</h3>
		        </div>
		        <div class="extra content">
		          
		            <span class="right floated"><i class="icon options"></i>  {{$list->count()}}</span>
		        </div>
		    </a>
		    <a href="/home" class="ui card">
		        <div class="content">
		            <h3 class="header link">Sejarah Permohonan</h3>
		        </div>
		        <div class="extra content">
		           
		            <span class="right floated"><i class="icon options"></i>  {{$list->count()}}</span>
		        </div>
		    </a>
	    </div>
	</div>
	<br>
	<br>

<a class="ui blue left ribbon label">Senarai Permohonan</a>	
	<table class="ui celled table">
  <thead>
    <tr>
    <th>Bil.</th>
    <th>Nama Pemohon</th>
    <th>Nama Jabatan</th>
    <th>Nama Bahagian</th>
    <th>Tujuan/Nama Program</th>
    <th>Tarikh/Masa Program</th>
    <th>Lokasi Program</th>
    <th>No Pekerja</th>
    <th>No Tel Pejabat</th>
    <th>No Tel Bimbit</th>

  </tr></thead>
  <tbody>
  	@php ($i = 0)
  	@foreach($data as $key=>$list)
    <tr>
      <td data-label="">{{++$i}}</td>
      <td data-label="">{{$list->users->name}}</td>
      <td data-label="">{{$list->users->vw_staff_federateds->department_name}}</td>
      <td data-label="">{{$list->users->vw_staff_federateds->division_name}}</td>
      <td data-label="">{{$list->tujuanprogram}}</td>
      <td data-label="">{{$list->tarikhprogram}}</td>
      <td data-label="">{{$list->lokasiprogram}}</td>
      <td data-label="">{{$list->users->vw_staff_federateds->employeeCode}}</td>
      <td data-label="">{{$list->users->vw_staff_federateds->office_no}}</td>
      <td data-label="">{{$list->notelbimbit}}</td>
    </tr>   
    @endforeach
  </tbody>

</table>
</div>
   

@endsection
