@extends('ui::layouts.app')

@section('content')
<div class="ui segment">
    <a class="ui blue left ribbon label">Borang Permohonan Perkhidmatan Fotografi</a>
    <br>
    <br>
	<form action="{{url('simpanborang')}}" method="POST" class="ui form">
  {{ csrf_field() }}
    
  	<div class="field">
    <label>Nama Pemohon</label>
    <input type="text" name="namapemohon" placeholder="Nama Pemohon" value="{{ Auth::User()->name }}" readonly>
  	</div>
  	<br>
  	<form class="ui form">
  	<div class="field">
    <label>Nama Jabatan</label>
    <input type="text" name="namajabatan" placeholder="Nama Jabatan" value="{{ $user->department_name }}" readonly>
  	</div>
	
  <br>
  	<form class="ui form">
  	<div class="field">
    <label>Nama Bahagian</label>
    <input type="text" name="namabahagian" placeholder="Nama Bahagian" value="{{ $user->division_name }}" readonly>
  	</div>
  	
	<br>
	<div class="field">
    <label>Tujuan/Nama Program</label>
    <input type="text" name="tujuanprogram" placeholder="Tujuan/Nama Program">
  	</div>
	
  <br>
    <div class="ui calendar" id="standard_calendar">
    <div class="field">
    <label>Tarikh/Masa Program</label>
     <div class="ui input left icon">
      <i class="calendar icon"></i>
    <input type="text" name="date" placeholder="Tarikh/Masa">
  </div>
  </div>
</div>

  <br>
	<div class="field">
    <label>Lokasi Program</label>
    <input type="text" name="lokasiprogram" placeholder="Lokasi Program">
  	</div>
  
  <br>
  	<div class="field">
    <label>No Pekerja</label>
    <input type="text" name="nopekerja" placeholder="No Pekerja" value="{{ $user->employeeCode }}" readonly>
  	</div>
  
  <br>
  	<div class="field">
    <label>No Telefon Pejabat</label>
    <input type="text" name="notelpej" placeholder="No Telefon Pejabat" value="{{ $user->office_no}}" readonly>
  	</div>
  
  <br>
  	<div class="field">
    <label>No Telefon Bimbit</label>
    <input type="text" name="notelbimbit" placeholder="No Telefon Bimbit">
  	</div>
  	<button class="btn btn-primary" type="submit">Hantar</button>
	</form>
	</div>


</div>
   

@endsection

@push('script')

<script type="text/javascript">
  $('#standard_calendar').calendar();
</script>
@endpush